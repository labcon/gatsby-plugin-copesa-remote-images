const { createRemoteFileNode } = require(`gatsby-source-filesystem`)
const get = require('lodash/get')

const getPath = (node, path, ext = null) => {
  const value = get(node, path)
  return ext ? value + ext : value
}

const getAttrFromString = (str, node, attr) => {
  const regex = new RegExp('<' + node + ' .*?' + attr + '="(.*?)"', "gi")
  let result
  const res = []

  while ((result = regex.exec(str))) {
    res.push(result[1]);
  }
  return res;
}

// exports.sourceNodes = ({ actions, createNodeId, createContentDigest }) => {
//   const { createTypes } = actions
//   createTypes(`type File implements Node @infer {
//       url: String
//   }`)
//   // createTypes(`interface FileFilterInput @nodeInterface {
//   //     fields: {
//   //       contentlt: String
//   //     }
//   // }`)
//   console.log('HOLA HOLA EN CREATETYPES')
// }

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  createTypes(`
    type ContentImages implements Node {
      url: String!
      contentImageSource: String
    }
  `)
}

exports.createResolvers = ({
  actions,
  cache,
  createNodeId,
  createResolvers,
  store,
  reporter,
}) => {
  const { createNode } = actions
  createResolvers({
    ContentImages: {
      contentImageSource: {
        type: `String`,
        resolve(source) {
          return source.imgOrigin
        }
      },
      imageFile: {
        type: `File`,
        resolve(source) {
          // console.log('LA SOURCE', source)
          return createRemoteFileNode({
            url: source.url,
            store,
            cache,
            createNode,
            createNodeId,
            reporter,
          })
        }
      }
    }
  })
}

exports.onCreateNode = async ({ node, actions, getNode, store, cache, createNodeId, createContentDigest }, options) => {
  const { createNode, touchNode, createNodeField } = actions
  const {
    nodeType,
    imagePath,
    extractImageFromPathString,
    name = 'localImage',
    auth = {},
    ext = null,
    prepareUrl = null,
    keepMediaSizes = false,
    categoryid,
    loopPathArray
  } = options

  const createImageNodeOptions = {
    store,
    cache,
    createNode,
    createNodeId,
    auth,
    ext,
    name,
    prepareUrl,
    categoryid,
    loopPathArray,
    extractImageFromPathString,
    keepMediaSizes,
    getNode,
    touchNode,
    createNodeField,
    createContentDigest
  }

  if (node.internal.type === nodeType) {
    // checkea si alguna parte del path indica que el nodo es un array y aplica split en esa parte
    // category
    if (node.categories && node.categories[0] === categoryid) {

      let imagePathSegments = []
      if (imagePath) {
        if (imagePath.includes("[].")) {
          imagePathSegments = imagePath.split("[].")
        }
        if (imagePathSegments.length) {
          await createImageNodesInArrays(imagePathSegments[0], node, { imagePathSegments, ...createImageNodeOptions })
        } else {
          const url = getPath(node, imagePath, ext)
          await createImageNode(url, node, createImageNodeOptions, imagePath, 'image Path Segments')
        }
      }
      if (extractImageFromPathString) {
        const getPathString = getPath(node, extractImageFromPathString, ext)
        const getArrImgs = getAttrFromString(getPathString)
        await createImageNodesFromArray(getArrImgs, node, { ...createImageNodeOptions })
      }
      if (loopPathArray) {
        await createImageNodesFromArray(loopPathArray, node, { ...createImageNodeOptions })
      }
    }
  }
}

async function createImageNode(url, node, options, imgorigin, debug) {
  const { name, imagePathSegments, prepareUrl, cache, createNode, createNodeId, getNode, createImageNode, createContentDigest, createNodeField, ...restOfOptions } = options
  let fileNodeID

  if (!url) {
    return
  }

  if (typeof prepareUrl === 'function') {
    url = prepareUrl(url)
  }

  // const mediaDataCacheKey = `wordpress-media-${node.wordpress_id}`
  // const cacheMediaData = await cache.get(mediaDataCacheKey)

  // if (cacheMediaData && node.modified === cacheMediaData.modified) {

  //   const fileNode = getNode(cacheMediaData.fileNodeID)
  //   if (fileNode) {
  //     fileNodeID = cacheMediaData.fileNodeID
  //     touchNode({
  //       nodeId: fileNodeID
  //     })
  //   }
  // }

  if (!fileNodeID) {
    const encodedSourceUrl = encodeURI(url)

    try {
      const nodeId = createNodeId(`contentimage-${encodedSourceUrl}`)
      // console.log('debug:', debug)
      // console.log('imgorigin!!!', imgorigin)
      const fileNode = await createNode({
        imgOrigin: imgorigin,
        ...restOfOptions,
        id: nodeId,
        cache: cache,
        url: encodedSourceUrl,
        parentNodeId: node.id,
        internal: {
          type: `ContentImages`,
          content: JSON.stringify(encodedSourceUrl),
          contentDigest: createContentDigest(encodedSourceUrl),
        }
      })

      if (fileNode) {
        fileNodeID = fileNode.id
        node[`${name}___NODE`] = fileNode.id

        // createNodeField({
        //   fileNode,
        //   name: `ContentImageSourceField`,
        //   value: imgorigin
        // })

        // await cache.set(mediaDataCacheKey, {
        //   fileNodeID,
        //   modified: node.modified
        // })

      }
      return fileNode
    } catch (err) {
      console.log('gatsby-plugin-copesa-remote-images ERROR:', err)
    }
  }
  return node
}

async function createImageNodesFromArray(arr, node, options, imgorigin) {
  if (typeof arr === 'undefined' || typeof node === 'undefined') {
    return
  }
  const { ext } = options
  Array.isArray(arr)
    ? Promise.all(
      arr.map(itemPath => {
        const item = getPath(node, itemPath, ext)
        Array.isArray(item)
          ? Promise.all(
            item.map(subItemPath => createImageNodesFromArray(subItemPath, node, options, itemPath))
          )
          : createImageNode(item, node, options, itemPath, `Array en Array ${itemPath}`)
      })
    ) : createImageNode(arr, node, options, imgorigin, `No es array, Directo ${imgorigin}`)
}

// Recorre recursivamente objetos/arreglos
async function createImageNodesInArrays(path, node, options) {
  if (typeof path === 'undefined' || typeof node === 'undefined') {
    return
  }

  const { imagePathSegments, ext } = options

  const pathIndex = imagePathSegments.indexOf(path),
    isPathToLeafProperty = pathIndex === imagePathSegments.length - 1,
    nextValue = getPath(node, path, isPathToLeafProperty ? ext : null)

  //obtiene el parent del leaf property, si ya no es el valor de `node`

  let nextNode = node
  if (isPathToLeafProperty && path.includes('.')) {
    const pathToLastParent = path
      .split('.')
      .slice(0, -1)
      .join('.')
    nextNode = get(node, pathToLastParent)
  }
  return Array.isArray(nextValue)
    // llama recursivamente la función con el next path segmente para cada elemento del array
    ? Promise.all(
      nextValue.map(item => createImageNodesInArrays(imagePathSegments[pathIndex + 1], item, options))
    )
    // de lo contrario, maneja el nodo
    : createImageNode(nextValue, nextNode, options, path, 'else if nextValue')
}